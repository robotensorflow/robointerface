import axios from 'axios';
import {Client} from 'paho-mqtt'
import { Notify } from 'quasar'
import { TaskTimer } from 'tasktimer';

export default class RaspberryService{
    static address = '192.168.2.1';
    static url = 'http://192.168.2.1:5000';
    static port = '5000';
    client = new Client('192.168.2.1',9001,'app');
    posServo1 = 0;
    posServo2 = 0;
    timer = new TaskTimer(10);

    getPos1(){
        return this.posServo1;
    }
    getPos2(){
        return this.posServo2;
    }
    setPos1(pos){
        if(this.posServo1 < 20){
            this.posServo1 = 20;
        }else if(this.posServo1 > 180){
            this.posServo1 = 180;
        }else{
            this.posServo1 = pos;
        }
    }
    setPos2(pos){
        if(this.posServo2 < 0){
            this.posServo2 = 0;
        }else if(this.posServo2 > 130){
            this.posServo2 = 130;
        }else{
            this.posServo2 = pos;
        }
    }

    connect(){
        this.client = new Client('192.168.2.1',9001,'app');
        Notify.create({
            message: 'Tentando conectar em 192.168.2.1',
            color: 'positive'
        });
         this.client.connect({
             onSuccess: () => {
                 Notify.create({
                     message: 'Conectado',
                     color: 'positive'
                 });
             }
         })

    }
    sendMQTT(command){
        this.client.send("robo", command )
    }
    moveFpvLeft() {
        this.setPos1(this.getPos1()+1);
        var servo = 1;
        var pos = this.getPos1();
        var command = "servo" + servo + " " + pos;
        this.sendMQTT(command)
        console.log(command)
    }
    moveFpvRight() {
        this.setPos1(this.getPos1()-1);
        var servo = 1;
        var pos = this.getPos1();
        var command = "servo" + servo + " " + pos;
        this.sendMQTT(command)
        console.log(command)
    }
    moveFpvUp() {
        this.setPos2(this.getPos2()+1);
        var servo = 2;
        var pos = this.getPos2();
        var command = "servo" + servo + " " + pos;
        this.sendMQTT(command)
        console.log(command)
    }
    moveFpvDown() {
        this.setPos2(this.getPos2()-1);
        var servo = 2;
        var pos = this.getPos2();
        var command = "servo" + servo + " " + pos;
        this.sendMQTT(command)
        console.log(command)
    }
    startMovingFpvLeft(){
        this.timer.reset();
        this.timer.add(task => this.moveFpvLeft()).start();
    }
    startMovingFpvRight(){
        this.timer.reset();
        this.timer.add(task => this.moveFpvRight()).start();
        this.timer.emit()
    }
    startMovingFpvUp(){
        this.timer.reset();
        this.timer.add(task => this.moveFpvUp()).start();
    }
    startMovingFpvDown(){
        this.timer.reset();
        this.timer.add(task => this.moveFpvDown()).start();
    }
    stopMovingFpv(){
        this.timer.reset();
        this.timer.stop();
    }
    map(x, in_min, in_max, out_min, out_max) {
        var out = Math.abs((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
        if(out > 255){
            out = 255;
        }
        if(out < 100){
            out = 100;
        }
        return out;
    }
    setVelocity(velocidade) {
        var command1 = "velocidade " + Math.round(this.map(velocidade,0,2.5,0,255));
        console.log(command1);
        this.sendMQTT(command1);
    }

    statusMotor = {
        motor1 : "parar",
        motor2 : "parar",
        motor3 : "parar",
        motor4 : "parar"
    }

    atualizar(statusNovo){
        var comando = "";
        if(statusNovo.motor1 != this.statusMotor.motor1){
            comando = statusNovo.motor1 + " 1";
            this.sendMQTT(comando);
        }
        if(statusNovo.motor2 != this.statusMotor.motor2){
            comando = statusNovo.motor2 + " 2";
            this.sendMQTT(comando);
        }
        if(statusNovo.motor3 != this.statusMotor.motor3){
            comando = statusNovo.motor3 + " 3";
            this.sendMQTT(comando);
        }
        if(statusNovo.motor4 != this.statusMotor.motor4){
            comando = statusNovo.motor4 + " 4";
            this.sendMQTT(comando);
        }
        this.statusMotor = statusNovo;
    }
    moveCarLeft(velocidade) {
        console.log("car left ")
        var statusNovo = {
            motor1 : "frente",
            motor2 : "frente",
            motor3 : "frente",
            motor4 : "tras"
        }
        this.atualizar(statusNovo);
    }
    moveCarRight(velocidade) {
        console.log("car right ")
        var statusNovo = {
            motor1 : "tras",
            motor2 : "frente",
            motor3 : "frente",
            motor4 : "frente"
        }
        this.atualizar(statusNovo);
    }
    moveCarBackwardRight(velocidade) {
        console.log("car back right ")
        var statusNovo = {
            motor1 : "tras",
            motor2 : "tras",
            motor3 : "tras",
            motor4 : "frente"
        }
        this.atualizar(statusNovo);
    }
    moveCarBackwardLeft(velocidade) {
        console.log("car back left ")
        var statusNovo = {
            motor1 : "frente",
            motor2 : "tras",
            motor3 : "tras",
            motor4 : "tras"
        }
        this.atualizar(statusNovo);
    }
    moveCarForward(velocidade) {
        console.log("car forward ")
        var statusNovo = {
            motor1 : "frente",
            motor2 : "frente",
            motor3 : "frente",
            motor4 : "frente"
        }
        this.atualizar(statusNovo);
    }
    moveCarBackward(velocidade) {
        console.log("car backward ")
        var statusNovo = {
            motor1 : "tras",
            motor2 : "tras",
            motor3 : "tras",
            motor4 : "tras"
        }
        this.atualizar(statusNovo);
    }
    moveCarStop() {
        console.log("car stop ")
        var statusNovo = {
            motor1 : "parar",
            motor2 : "parar",
            motor3 : "parar",
            motor4 : "parar"
        }
        this.atualizar(statusNovo);
    }
}