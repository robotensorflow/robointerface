mkdir prod
PATH D:\Dev\android-sdk-eclipse\build-tools\28.0.3
zipalign -f -v 4 src-cordova\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk prod/app.apk
apksigner sign --ks prod/prod.keystore --ks-key-alias prod --ks-pass pass:projetovtnt prod/app.apk